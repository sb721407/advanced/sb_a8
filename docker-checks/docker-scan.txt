
Testing petrakimovdocker/skillboxapp...

✗ Low severity vulnerability found in openssl/libcrypto1.1
  Description: CVE-2023-0464
  Info: https://security.snyk.io/vuln/SNYK-ALPINE314-OPENSSL-3368739
  Introduced through: openssl/libcrypto1.1@1.1.1t-r0, openssl/libssl1.1@1.1.1t-r0, apk-tools/apk-tools@2.12.7-r0, libretls/libretls@3.3.3p1-r3
  From: openssl/libcrypto1.1@1.1.1t-r0
  From: openssl/libssl1.1@1.1.1t-r0 > openssl/libcrypto1.1@1.1.1t-r0
  From: apk-tools/apk-tools@2.12.7-r0 > openssl/libcrypto1.1@1.1.1t-r0
  and 4 more...
  Fixed in: 1.1.1t-r1



Package manager:   apk
Project name:      docker-image|petrakimovdocker/skillboxapp
Docker image:      petrakimovdocker/skillboxapp
Platform:          linux/amd64
Base image:        alpine:3.14.9

Tested 14 dependencies for known vulnerabilities, found 1 vulnerability.

According to our scan, you are currently using the most secure version of the selected base image

For more free scans that keep your images secure, sign up to Snyk at https://dockr.ly/3ePqVcp

-------------------------------------------------------

Testing petrakimovdocker/skillboxapp...

✗ Medium severity vulnerability found in golang.org/x/sys/unix
  Description: Incorrect Privilege Assignment
  Info: https://security.snyk.io/vuln/SNYK-GOLANG-GOLANGORGXSYSUNIX-3310442
  Introduced through: golang.org/x/sys/unix@v0.0.0-20210603081109-ebe580a85c40
  From: golang.org/x/sys/unix@v0.0.0-20210603081109-ebe580a85c40
  Fixed in: 0.1.0

✗ High severity vulnerability found in github.com/prometheus/client_golang/prometheus/promhttp
  Description: Denial of Service (DoS)
  Info: https://security.snyk.io/vuln/SNYK-GOLANG-GITHUBCOMPROMETHEUSCLIENTGOLANGPROMETHEUSPROMHTTP-2401819
  Introduced through: github.com/prometheus/client_golang/prometheus/promhttp@v1.11.0
  From: github.com/prometheus/client_golang/prometheus/promhttp@v1.11.0
  Fixed in: 1.11.1



Package manager:   gomodules
Target file:       /app
Project name:      github.com/bhavenger/skillbox-diploma
Docker image:      petrakimovdocker/skillboxapp

Tested 40 dependencies for known vulnerabilities, found 2 vulnerabilities.

For more free scans that keep your images secure, sign up to Snyk at https://dockr.ly/3ePqVcp


Tested 2 projects, 2 contained vulnerable paths.



